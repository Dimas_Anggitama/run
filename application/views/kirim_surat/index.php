<!-- Include jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

	<div class="row">
        <div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<button type="button" class="btn btn-info pull-right" onclick="location.href='<?php echo base_url($this->uri->rsegment(1).'/edit/')?>';"><?php echo $lang_add;?></button>
				</div>
				<!-- /.box-header -->
				<?php if ($this->session->flashdata('message')) { ?>
					<script>
						$(document).ready(function() {
							$('#modal_message').modal('show');
						});
					</script>
					
					<!-- Modal -->
					<div class="modal fade" id="modal_message" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title text-center text-success text-bold"><?php echo $lang_pemberitahuan; ?></h4>
								</div>
								<div class="modal-body">
									<p class="text-center"><?php echo $this->session->flashdata('message'); ?></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>

						</div>
					</div>
				<?php } ?>
				<div class="box-body">
					<?php if (!empty($this->session->flashdata('status'))) { ?>
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata('status');?>!
						</div>
					<?php } ?>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><div align="center">No</div></th>
								<th><div align="center">Nomor Naskah</div></th>
								<th><div align="center">Tanggal Naskah</div></th>
								<th><div align="center">Sifat Kecepatan Penyampaian</div></th>
								<th><div align="center">Jenis</div></th>
								<th><div align="center">Dari</div></th>
								<th><div align="center">Perihal</div></th>
								<th><div align="center">File Surat</div></th>
								<th><div align="center">Tanggal Diterima</div></th>
								<th><div align="center">#Aksi#</div></th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php foreach ($content as $val): ?>
								<tr>
									<td><div align="center" ><?php echo $no;?></div></td>
									<td><div align="center" ><?php echo $val->nomor_naskah;?></div></td>
									<td><div align="center" ><?php echo date('d-m-Y', strtotime($val->tgl_naskah));?></div></td>
									<td><div align="center" ><?php echo $nama_prioritas[$no]; ?></div></td>
									<td><div align="center" ><?php echo $nama_sifat_kategori[$no]; ?></div></td>
									<td><div align="center" ><?php echo $val->dari_naskah;?></div></td>
									<td><div align="center" ><?php echo $val->perihal_naskah;?></div></td>
									<td><div align="center" >
										<?php if ($val->file_surat!=''){?>
											<a class="btn btn-xs btn-warning" target="_blank" download href="<?php echo base_url('uploads/'.$val->file_surat);?>" title="<?php echo $lang_download;?>"><i class="fa fa-download"></i></a>
											<a class="btn btn-xs btn-info" target="_blank" preview href="<?php echo base_url('uploads/'.$val->file_surat);?>" title="<?php echo $lang_view;?>"><i class="fa fa-eye"></i></a>
										<?php } else {
											echo $lang_no_file;
										} ?>
										</div>
									</td>
									<td><div align="center" ><?php echo date('d-m-Y',strtotime($val->tgl_diterima));?></div></td>
									<td align="center">
										<a data-toggle="modal" data-target="#modal_suratMasuk-detail-<?php echo $no; ?>" href="#" title="<?php echo $lang_detail;?>" class="btn btn-xs btn-info"><i class="fa fa-info-circle"></i></a>
										<a data-toggle="modal" data-target="#modal_alert-disposisi-<?php echo $no; ?>" href="#" title="<?php echo $lang_disposisi.' ke kepala kantor'; ?>" class="btn btn-xs btn-success"><i class="fa fa-send"></i></a>
										<!--<a onclick="return confirm('<?php //echo $lang_are_you_sure;?>?');" href="<?php //echo base_url($this->uri->rsegment(1).'/disposisi/'.$val->id_agenda);?>" title="<?php //echo $lang_disposisi.' ke kepala kantor'; ?>" class="btn btn-xs btn-success"><i class="fa fa-send"></i></a>-->
										
										<!-- Modal (pop up) for disposisi surat masuk ke kepala kantor-->
										<div class="modal fade" id="modal_alert-disposisi-<?php echo $no; ?>" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true" class="text-danger">&times;</span>
														</button>
														<h4 class="modal-title text-center text-warning text-bold"><?php echo $lang_peringatan; ?></h4> 
														
														<!-- Catatan: 	Agar tampilan Judul modal/pop up-nya bisa sebaris dengan tombol "Close", maka letak baris code-nya harus di bawahnya baris code tombol "Close". -->
														<!-- 			Karena kalau sebaliknya, maka yang terjadi pada tampilannya adalah judul modal/pop up berada di atasnya tombol "Close". -->
													</div>													
													<div class="modal-body">
														<p class="text-center">
															<h4 class="text-bold text-center">Apakah anda yakin ingin melakukan Disposisi?</h4>
															<h5 class="text-center">Data yang telah didisposisikan tidak dapat diubah atau dihapus kembali.</h5>
														</p>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-danger text text-right" data-dismiss="modal" aria-label="Close">Tutup</button>
														<a href="<?php echo base_url($this->uri->rsegment(1).'/disposisi/'.$val->id_agenda); ?>" class="btn btn-primary text text-right"><b><?php echo $lang_confirm; ?></b></a>
													</div>
												</div>
											</div>
										</div>
										
										<!-- Modal (pop up) for Edit data detail surat masuk -->
										<div class="modal fade" id="modal_suratMasuk-detail-<?php echo $no; ?>" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
											<div class="modal-dialog modal-dialog-centered" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="fa fa-info-circle" id="modalCenterTitle"><?php echo $lang_detail; ?></h5>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true" class="text-danger">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<div class="container-fluid text-left">
															<div class="row">
																<div class="col-sm-10">
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Nomor Naskah</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $val->nomor_naskah; ?></b></h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Tanggal Naskah</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo date('d-m-Y', strtotime($val->tgl_naskah));?></b></h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Dari</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $val->dari_naskah; ?></b></h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Perihal</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $val->perihal_naskah; ?></b></h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Lampiran</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $val->lampiran_naskah; ?></b></h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Tingkat kerahasiaan</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $nama_sifat_kategori[$no]; ?></b></h4>	<!-- sifat -->
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Sifat kecepatan penyampaian</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $nama_prioritas[$no]; ?></b></h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Jenis surat</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $nama_jenis_kategori[$no]; ?></b></h4>	<!-- jenis -->
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Status</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: <b><?php echo $nama_status[$no]; ?></b></h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>File surat</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4>: 
																				<?php if ($val->file_surat!='') { ?>
																					<b>
																						<a class="btn btn-xs btn-warning" target="_blank" download href="<?php echo base_url('uploads/'.$val->file_surat); ?>" title="<?php echo $lang_download;?>"><i class="fa fa-download"></i></a>
																						<a class="btn btn-xs btn-info" target="_blank" preview href="<?php echo base_url('uploads/'.$val->file_surat); ?>" title="<?php echo $lang_view;?>"><i class="fa fa-eye"></i></a>
																					</b>
																				<?php } else { ?>
																					<b class="text-danger"> <?php echo $lang_no_file; ?> </b>
																				<?php } ?>
																			</h4>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-8 col-sm-6">
																			<h4>Tanggal diterima</h4>
																		</div>
																		<div class="col-4 col-sm-6">
																			<h4 class="text-success">: <b><?php echo date('d-m-Y', strtotime($val->tgl_diterima)); ?></b></h4>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<div class="col-xs-6 text-left">
															<a onclick="return confirm('<?php echo $lang_are_you_sure;?>?');" href="<?php echo base_url($this->uri->rsegment(1).'/delete/'.$val->id_agenda); ?>" class="btn btn-danger text text-left"><?php echo $lang_delete; ?></a>
														</div>
														<div class="col-xs-6 text-right">
															<a href="<?php echo base_url($this->uri->rsegment(1).'/edit/'.$val->id_agenda); ?>" class="btn btn-primary text text-right"><?php echo $lang_edit; ?></a>
															<button type="button" class="btn btn-success text text-right" data-dismiss="modal" aria-label="Close"><b><u>Tutup</u></b></button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<?php $no++; ?>
							<?php endforeach; ?>	                	
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
          <!-- /.box -->
      
        <!-- /.col -->
      </div>
 </div>    