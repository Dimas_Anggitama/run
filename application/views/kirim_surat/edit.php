<!-- Include jQuery -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<div class="box">
				<div class="box-header"></div>
				<!-- /.box-header -->
				<?php if ($message) { ?>
					<script>
						$(document).ready(function(){
							$("#modal_message").modal('show');
						});
					</script>
					
					<!-- Modal -->
					<div class="modal fade" id="modal_message" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title text-center text-danger text-bold"><?php echo $lang_kesalahan; ?></h4>
								</div>
								<div class="modal-body">
									<p class="text-center"><?php echo $message; ?></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>

						</div>
					</div>
				<?php } ?>
				<?php echo form_open_multipart('','class="form-horizontal"'); ?>
				<div class="box-body">
					<?php echo validation_errors(); ?>
					<?php foreach ($contentToEdit as $val): ?>
				
						<!--Nomor Naskah Dinas-->
						<div class="form-group">
							<label for="input_nomor_naskah" class="col-sm-4 control-label"><?php echo $lang_naskahdinas_surat;?></label>
							<div class="col-sm-8">
								<input type="text" name="nomor_naskah" class="form-control" id="input_nomor_naskah" placeholder="<?php echo $lang_naskahdinas_surat;?>" value="<?php echo $val->nomor_naskah; ?>" required>
							</div>
						</div>
			  
						<!--Tanggal Naskah Dinas-->
						<div class="form-group">
							<label for="input_tanggal_naskah" class="col-sm-4 control-label"><?php echo $lang_tgldinas_surat;?></label>
							<div class="col-sm-8">
								<?php if ($contentToEdit[0] == null) { ?>
									<input id="input_tanggal_naskah" type="date" name="tanggal_naskah" class="form-control datepicker" required>
									<!--<input id="input_tanggal_naskah" type="date" name="tanggal_diterima" class="form-control"  value="<?php //echo date('d-m-Y'); ?>" readonly>-->
								<?php } else { ?>
									<?php foreach ($contentToEdit as $val): ?>
										<input id="input_tanggal_naskah" type="date" name="tanggal_naskah" class="form-control datepicker" value="<?php echo $val->tgl_naskah; ?>" required>
									<?php endforeach; ?>
								<?php } ?>
							</div>
						</div>
						
						<!--Perihal-->
						<div class="form-group">
							<label for="input_perihal_naskah" class="col-sm-4 control-label"><?php echo $lang_hal_surat;?></label>
							<div class="col-sm-8">
								<input type="text" name="perihal_naskah" class="form-control" id="input_perihal_naskah" placeholder="<?php echo $lang_hal_surat;?>" value="<?php echo $val->perihal_naskah; ?>" required>
							</div>
						</div>
			  
						<!--Dari-->
						<div class="form-group">
							<label for="input_dari_naskah" class="col-sm-4 control-label"><?php echo $lang_dari_surat;?></label>
							<div class="col-sm-8">
								<input type="text" name="dari_naskah" class="form-control" id="input_dari_naskah" placeholder="<?php echo $lang_dari_surat;?>" value="<?php echo $val->dari_naskah; ?>" required>
							</div>
						</div>
						
					<?php endforeach; ?>

					<!--Status-->
					<div class="form-group">
						<label for="input_status" class="col-sm-4 control-label"><?php echo $lang_status ?></label>
						<div class="col-sm-8">
							<select class="form-control" id="input_status" name="status" required>
								<?php if ($contentToEdit[0] == null) { ?>
									<option value=""><?php echo $lang_choose; ?></option>
								<?php } else { ?>
									<?php foreach ($contentToEdit as $val): ?>
										<option class="bg-danger" value="<?php echo $val->id_status; ?>"> <?php echo $contentToEdit_namaStatus; ?></option>
									<?php endforeach; ?>
								<?php } ?>
								<?php foreach ($induk_status as $val): ?>
									<option value="<?php echo $val->id_status; ?>" <?php echo $val->id_status==$content->id_status?'selected':''; ?> >
										<?php echo $val->nama_status; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
							
					<!-- Sifat-> Kecepatan Penyampaian-->
					<div class="form-group">
						<label for="prioritas_kategori" class="col-sm-4 control-label"><?php echo $lang_prioritas_surat; ?></label>
						<div class="col-sm-8">
							<select class="form-control" id="prioritas_kategori" name="prioritas_kategori" required>
								<?php if ($contentToEdit[0] == null) { ?>
									<option value=""><?php echo $lang_choose; ?></option>
								<?php } else { ?>
									<?php foreach ($contentToEdit as $val): ?>
										<option class="bg-danger" value="<?php echo $val->id_prioritas; ?>"> <?php echo $contentToEdit_namaPrioritas; ?></option>
									<?php endforeach; ?>
								<?php } ?>
								<?php foreach ($induk_prioritas as $val):?>
									<option value="<?php echo $val->id_prioritas; ?>" <?php echo $val->id_prioritas==$content->id_prioritas?'selected':'';?>>
										<?php echo $val->nama_prioritas; ?>
									</option>
								<?php endforeach; ?>
							</select>
							<?php //foreach ($induk_sifat2 as $val):?>
								<!--<label>-->
									<?php //$i=0; echo '<input type="radio" name="sifat2_kategori" value="'.$val->id_sifat2.'" ' . ($i==0 ? "checked" : "") . ' /> ';
									//$i++; ?>
									<?php //echo $val->nama_sifat2?>
								<!--</label>-->
							<?php //endforeach;?>
						</div>
					</div>
		  
					<!-- Jenis -->
					<div class="form-group">
						<label for="input_jenis_kategori" class="col-sm-4 control-label"><?php echo $lang_jenis_surat?></label>
						<div class="col-sm-8">
							<select class="form-control" id="input_jenis_kategori" name="jenis_kategori" required>
								<?php if ($contentToEdit[0] == null) { ?>
									<option value=""><?php echo $lang_choose; ?></option>
								<?php } else { ?>
									<?php foreach ($contentToEdit as $val): ?>
										<option class="bg-danger" value="<?php echo $val->id_jenis_kategori; ?>"> <?php echo $contentToEdit_namaJenisKategori; ?></option>
									<?php endforeach; ?>
								<?php } ?>
								<?php foreach ($induk_jenis as $val):?>
									<option value="<?php echo $val->id_jenis_kategori?> "<?php echo $val->id_jenis_kategori==$content->id_jenis_kategori?'selected':'';?>>
										<?php echo $val->nama_jenis_kategori; ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
		  
					<!-- Sifat (Tingkat keamanan) -->
					<div class="form-group">
						<label for="input_sifat_kategori" class="col-sm-4 control-label"><?php echo $lang_sifat_surat?></label>
						<div class="col-sm-8">
							<select class="form-control" id="input_sifat_kategori" name="sifat_kategori" required>
								<?php if ($contentToEdit[0] == null) { ?>
									<option value=""><?php echo $lang_choose; ?></option>
								<?php } else { ?>
									<?php foreach ($contentToEdit as $val): ?>
										<option class="bg-danger" value="<?php echo $val->id_sifat_kategori; ?>"> <?php echo $contentToEdit_namaSifatKategori; ?></option>
									<?php endforeach; ?>
								<?php } ?>
								<?php foreach ($induk_sifat as $val):?>
									<option value="<?php echo $val->id_sifat_kategori; ?> "<?php echo $val->id_sifat_kategori==$content->id_sifat_kategori?'selected':'';?>>
										<?php echo $val->nama_sifat_kategori; ?>
									</option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					
					<?php foreach ($contentToEdit as $val): ?>
						<!--Lampiran-->
						<div class="form-group">
							<label for="input_lampiran_naskah" class="col-sm-4 control-label"><?php echo $lang_lampiran_surat;?></label>
							<div class="col-sm-8">
								<input type="text" name="lampiran_naskah" class="form-control" id="input_lampiran_naskah" placeholder="<?php echo $lang_lampiran_surat;?>" value="<?php echo $val->lampiran_naskah?>" required>
							</div>
						</div>
						
						<!--File surat-->
						<!--  <?php if ($content->file_surat!=''){?>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label"><?php echo $lang_filesurat;?></label>
							<div class="col-sm-8">
								</div>
							</div>
						<?php }?>
					  
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label"><?php echo $lang_filesurat;?></label>
								<div class="col-sm-8">
									<input type="file" name="file_surat" class="" id="exampleInputFile">
								</div>
							</div>
						-->
						<?php if ($content->file_surat!='') { ?>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label"><?php echo $lang_file_surat; ?></label>
								<div class="col-sm-8">
									<a class="btn btn-xs btn-warning" target="_blank" download href="<?php echo base_url('uploads/'.$content->file_surat);?>"><i class="fa fa-download"></i> <?php echo $lang_download;?></a>
									<a class="btn btn-xs btn-info" target="_blank" href="<?php echo base_url('uploads/'.$content->file_surat);?>"><i class="fa fa-eye"></i> <?php echo $lang_view;?></a>
								</div>
							</div>
						<?php } ?>
						<div class="form-group">
						  <label for="input_file" class="col-sm-4 control-label"><?php echo $lang_berkas_surat; ?></label>
							<div class="col-sm-8">
								<input type="file" name="file_surat" class="" id="input_file" accept=".pdf" value="<?php echo $val->file_surat; ?>" required>
							</div>
						</div>
						
						<!--Tanggal Diterima-->
						<div class="form-group">
							<label for="input_tanggal_diterima" class="col-sm-4 control-label"><?php echo $lang_tanggalditerima_surat;?></label>
							<div class="col-sm-8">
								<?php if ($contentToEdit[0] == null) { ?>
									<input type="date" id="input_tanggal_diterima" name="tanggal_diterima" class="form-control"  value="<?php echo date('Y-m-d'); ?>" readonly>
								<?php } else { ?>
									<?php foreach ($contentToEdit as $val): ?>
										<input type="date" id="input_tanggal_diterima" name="tanggal_diterima" class="form-control"  value="<?php echo date('Y-m-d', strtotime($val->tgl_diterima)); ?>" readonly>
									<?php endforeach; ?>
								<?php } ?>
								<!--
									<input type="text" name="tanggal" class="form-control"  value="<?php //echo $content->date_create_surat==''?date('Y-m-d H:i:s'):date('Y-m-d H:i:s',strtotime($content->date_create_surat));?>" required>
									<input type="text" name="year" class="form-control"  value="<?php //echo date('Y'); ?>" disabled required>
								-->
							</div>
						</div>
						
						<!-- /.box-body -->
			
					<!-- /.box-footer -->
			  
					<?php endforeach; ?>
				</div>
				<div class="box-footer">
					<button type="submit" name="surat" value="surat" class="btn btn-info pull-right"><?php echo $lang_save;?></button>
				</div>
				<?php echo form_close();?>
				
				<!-- /.box -->
			</div>
			
          <!--col md 6 -->
        </div> 
		
        <!-- row -->
    </div>
     <!--  </div> -->
     <!--  <div class="col-md-3">
      </div> -->
<!-- </div> -->
