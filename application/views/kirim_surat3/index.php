	<div class="row">
        <div class="col-md-12">

          <div class="box">
            <div class="box-header">
              <button type="button" class="btn btn-info pull-right" onclick="location.href='<?php echo base_url($this->uri->rsegment(1).'/edit/')?>';"><?php echo $lang_add;?></button>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if (!empty($this->session->flashdata('status'))){?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata('status');?>!
              </div>
             <?php }?>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><div align="center">No</div></th>
                  <th><div align="center">Nomor Naskah</div></th>
                  <th><div align="center">Tanggal Naskah</div></th>
                  <th><div align="center">Lampiran</div></th>
                  <th><div align="center">Sifat</div></th>
                  <th><div align="center">Sifat Kecepatan Penyampaian</div></th>
                  <th><div align="center">Jenis</div></th>
                  <th><div align="center">Status</div></th>
                  <th><div align="center">Dari</div></th>
                  <th><div align="center">Perihal</div></th>
                  <th><div align="center">File Surat</div></th>
                  <th><div align="center">Tanggal Diterima</div></th>
                  <th><div align="center">Nomor Agenda</div></th>
				  <th><div align="center">Tahun Surat</div></th>
				  <th><div align="center">Tipe Surat</div></th>
				  <th><div align="center">Tracking</div></th>
				  <th><div align="center">#Aksi#</div></th>
                </tr>
                </thead>
                	<tbody>
                	<?php 
                	$no=1;
                	foreach ($content as $val):?>
					<?php if (($this->session->userdata('rulesID_account') != 1) &
							($this->session->userdata('rulesID_account') != 4) &
							($this->session->userdata('rulesID_account') != 5) &
							($this->session->userdata('rulesID_account') != 6) &
							($this->session->userdata('rulesID_account') != 7) &
							($this->session->userdata('rulesID_account') != 9) &
							($this->session->userdata('rulesID_account') != 13)) {
						continue;
					} ?>
						<tr>
							<td><div align="center" ><?php echo $no;?></div></td>
							<td><div align="center" ><?php echo $val->nomor_naskah;?></div></td>
							<td><div align="center" ><?php echo date('d-m-Y H:i:s',strtotime($val->tgl_naskah)); ?></div></td>
              <td><div align="center" ><?php echo $val->lampiran_naskah;?></div></td>
							<td><div align="center" ><?php
$sql = "SELECT * FROM ap_surat_sifat WHERE id_sifat_kategori=?";
$query = $this->db->query($sql,array($val->id_sifat_kategori));
$row = $query->row();
if (isset($row))
{
    echo $row->nama_sifat_kategori;
}
?></div></td>
              <td><div align="center" ><?php
$sql = "SELECT * FROM ap_surat_sifat2 WHERE id_sifat2=?";
$query = $this->db->query($sql,array($val->id_sifat2));
$row = $query->row();
if (isset($row))
{
    echo $row->nama_sifat2;
}
?></div></td>
              <td><div align="center" ><?php
$sql = "SELECT * FROM ap_surat_jenis WHERE id_jenis_kategori=?";
$query = $this->db->query($sql,array($val->id_jenis_kategori));
$row = $query->row();
if (isset($row))
{
    echo $row->nama_jenis_kategori;
}
?></div></td>
              <td><div align="center" ><?php
$sql = "SELECT * FROM ap_surat_status WHERE id_status=?";
$query = $this->db->query($sql,array($val->id_status));
$row = $query->row();
if (isset($row))
{
    echo $row->nama_status;
}
?></div></td>
              <td><div align="center" ><?php echo $val->dari_naskah;?></div></td>
              <td><div align="center" ><?php echo $val->perihal_naskah;?></div></td>
              <td><div align="center" >
                <?php if ($val->file_surat!=''){?>
                  <a class="btn btn-xs btn-warning" target="_blank" download href="<?php echo base_url('uploads/'.$val->file_surat);?>" title="<?php echo $lang_download;?>"><i class="fa fa-download"></i></a>
                  <a class="btn btn-xs btn-info" target="_blank" preview href="<?php echo base_url('uploads/'.$val->file_surat);?>" title="<?php echo $lang_view;?>"><i class="fa fa-eye"></i></a>
                          <?php } else {echo $lang_no_file;}?>
                </div></td>
				
				
				<td><div align="center" ><?php echo date('d-m-Y H:i:s',strtotime($val->date_create_surat));?></div></td>
				<td><div align="center" ><?php echo $val->nomor_agenda;?></div></td>
				<td><div align="center" ><?php echo $val->tahun_surat;?></div></td>
				<td><div align="center" ></div></td>
				<td>
					<div align="center" >
						<a data-toggle="modal" href="#modal_tracking-row-<?php echo $val->id_agenda; ?>" title="<?php echo $lang_disposisi;?>" class="btn btn-xs btn-info"><i class="fa fa-save"></i></a>
					</div>
					
					
					<div class="modal fade" id="modal_tracking-row-<?php echo $val->id_agenda; ?>">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 align="center" class="modal-title">Pohon Tracking :</h4>
								</div>
								<?php echo form_open_multipart('','class="form-horizontal"');?>
			 
								<div class="modal-body">
									
									<?php if($this->session->userdata('rulesID_account') == '6'){?>
										<div><b class="mdi mdi-checkbox-blank-circle text-danger"></b> (datetime) Surat didisposisikan ke KASI</div>
										<div class="mdi mdi-ray-end-arrow mdi-rotate-90"></div>
										<div class="mdi mdi-check-circle"> <?php echo date('d-m-Y H:i:s',strtotime($val->tgl_naskah));?> Surat Masuk ke Kepala Kantor</div>
									
									<?php }else{ ?>
										<div><b class="mdi mdi-checkbox-blank-circle text-danger"></b> (datetime) Surat didisposisikan ke Pelaksana </div>
										<div class="mdi mdi-ray-end-arrow mdi-rotate-90"></div>
										<div class="mdi mdi-check-circle"> (datetime) Surat didisposisikan ke KASI</div>
										<div class="mdi mdi-ray-end-arrow mdi-rotate-90"></div>
										<div class="mdi mdi-check-circle"> <?php echo date('d-m-Y H:i:s',strtotime($val->tgl_naskah));?> Surat Masuk ke Kepala Kantor</div>
									<?php } ?>
								</div>
					  
								<div class="modal-footer">
									<button id="btnIsMicro" type="button" class="btn btn-square btn-danger" data-dismiss="modal" title="<?php echo 'Close';?>">Close&nbsp;&nbsp;<i class="fa fa-close"></i></button>
									<button type="submit" name="surat" value="surat" class="btn btn-info pull-right"><?php echo $lang_save;?></button>
								</div>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</td>
				
				<td>
					<div align="center">
						<a data-toggle="modal" href="#modal_disposisi-row-<?php echo $val->id_agenda; ?>" title="<?php echo $lang_disposisi;?>" class="btn btn-xs btn-info"><i class="fa fa-save"></i></a>
										
						<a onclick="return confirm('<?php echo $lang_are_you_sure;?>?');" href="<?php echo base_url($this->uri->rsegment(1).'/delete/'.$val->id_agenda);?>" title="<?php echo $lang_delete;?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
					</div>
								
					<div class="modal fade" id="modal_disposisi-row-<?php echo $val->id_agenda; ?>">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 align="center" class="modal-title">Disposisi ke pelaksana</h4>
								</div>
								<form action="#" method="post">
									<div class="modal-body">
										<div>
											<input type="radio" name="pel_dalam" value="2">Pelaksana Dalam<br>
											<input type="radio" name="pel_hanggar" value="3">Pelaksana Hanggar<br>
											<input id="pel_pic" name="pel_pic" type="radio" value="8">
											<label for="pel_pic">Pelaksana PIC</label>
											<input id="pel_cukai" name="pel_cukai" type="radio" value="11">
											<label for="pel_cukai">Pelaksana Cukai</label>
											<input id="pel_ki" name="pel_ki" type="radio" value="12">
											<label for="pel_ki">Pelaksana KI</label>
										</div>
										
										
  <input type="radio" name="gender" value="male"> Male<br>
  <input type="radio" name="gender" value="female"> Female<br>
  <input type="radio" name="gender" value="other"> Other<br>  
									</div>
						  
									<div class="modal-footer">
										<button id="btnIsMicro" type="button" class="btn btn-square btn-danger" data-dismiss="modal" title="<?php echo 'Close';?>">Close&nbsp;&nbsp;<i class="fa fa-close"></i></button>
										<a href="<?php echo base_url($this->uri->rsegment(1).'/delete/'.$val->id_agenda);?>" type="submit" name="surat" value="surat" class="btn btn-info pull-right"><?php echo 'Konfirmasi'?></a>
									</div>
								</form>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
				</td>
			</tr>
					<?php 
						$no++;
						endforeach;?>	                	
                	</tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
      
        <!-- /.col -->
    </div>
 </div>    