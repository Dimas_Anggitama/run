<div class="row">
    <div class="col-md-12">

        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <?php echo validation_errors();?>
              <?php if (!empty($this->session->flashdata('status'))){?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon fa fa-ban"></i> <?php echo $this->session->flashdata('status');?>!
              </div>
             <?php }?>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><div align="center">No</div></th>
                  <th><div align="center">Nomor Naskah</div></th>
                  <th><div align="center">Tanggal Naskah</div></th>
                  <th><div align="center">Lampiran</div></th>
                  <th><div align="center">Sifat</div></th>
                  <th><div align="center">Sifat Kecepatan Penyampaian</div></th>
                  <th><div align="center">Jenis</div></th>
                  <th><div align="center">Status</div></th>
                  <th><div align="center">Dari</div></th>
                  <th><div align="center">Perihal</div></th>
                  <th><div align="center">File Surat</div></th>
                  <th><div align="center">Tanggal Diterima</div></th>
                  <th><div align="center">Nomor Agenda</div></th>
				  <th><div align="center">Tahun Surat</div></th>
				  <th><div align="center">Tipe Surat</div></th>
				  <th><div align="center">Tracking</div></th>
				  <th><div align="center">#Aksi#</div></th>
                </tr>
                </thead>
                	<tbody>
                	<?php 
                	$no=1;
                	foreach ($content as $val):?>
					<?php if ($val->tanggal_arsip != null) { ?>
						
						<tr>
							<td><div align="center" ><?php echo $val->id_agenda; //echo $no;?></div></td>
							<td><div align="center" ><?php echo $nomor_naskah[$val->id_agenda];?></div></td>
							<td><div align="center" ><?php echo date('d-m-Y H:i:s',strtotime($tgl_naskah[$val->id_agenda]));?></div></td>
							<td><div align="center" ><?php echo $lampiran_naskah[$val->id_agenda];?></div></td>
							<td><div align="center" ><?php echo $id_sifat_kategori[$val->id_agenda]?></div></td>
							<td><div align="center" ><?php echo $id_sifat2[$val->id_agenda]?></div></td>
							<td><div align="center" ><?php echo $id_jenis_kategori[$val->id_agenda]?></div></td>
							<td><div align="center" ><?php echo $id_status[$val->id_agenda]?></div></td>
							<td><div align="center" ><?php echo $dari_naskah[$val->id_agenda];?></div></td>
							<td><div align="center" ><?php echo $perihal_naskah[$val->id_agenda];?></div></td>
							<td>
								<div align="center" ><?php  
									if ($file_surat[$val->id_agenda]!=''){?>
										<a class="btn btn-xs btn-warning" target="_blank" download href="<?php echo base_url('uploads/'.$file_surat[$val->id_agenda]);?>" title="<?php echo $lang_download;?>"><i class="fa fa-download"></i></a>
										<a class="btn btn-xs btn-info" target="_blank" preview href="<?php echo base_url('uploads/'.$file_surat[$val->id_agenda]);?>" title="<?php echo $lang_view;?>"><i class="fa fa-eye"></i></a>
									<?php } else { 
										echo $lang_no_file;
									} ?>
								</div>
							</td>
							<td><div align="center" ><?php echo date('d-m-Y H:i:s',strtotime($date_create_surat[$val->id_agenda]));?></div></td>
							<td><div align="center" ><?php echo $nomor_agenda[$val->id_agenda];?></div></td>
							<td><div align="center" ><?php echo $tahun_surat[$val->id_agenda];?></div></td>
							<td><div align="center" ><?php echo $val->uraian_jenis_surat?></div></td>
							<td>
								<div align="center" >							
									<a data-toggle="modal" href="#modal_tracking_row-<?php echo $val->id_agenda;?>-<?php echo str_replace(" ", "", strtolower($val->keberadaan_surat));?>" title="<?php echo 'Tracking';?>" class="btn btn-xs <?php echo $warna[$val->id_agenda];?>">
										<?php echo str_replace("(...)", $val->keberadaan_surat, strtolower($uraian_tracking[$val->id_agenda]));?>
									</a>
								</div>
								<div class="modal fade" id="modal_tracking_row-<?php echo $val->id_agenda;?>-<?php echo str_replace(" ", "", strtolower($val->keberadaan_surat));?>">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												<h4 align="center" class="modal-title">Tracking</h4>
											</div>
											<div class="modal-body">
												<?php $tracking_row = 1; //row tracking, bukan row tabel.	
												
												//$result_row = 1;		
												//foreach ($tracking_basic[$val->id_agenda] as $res) { 
												$iteration = 1;
												//$keberadaan_surat[$count][$keberadaan_surat_row]
												//$keberadaan_surat[$val->id_jabatan][$iteration]
												foreach ($tracking_format[$val->id_agenda][$tracking_row] as $res) {
														
													//echo $val->keberadaan_surat; ?>
													<?php //if ($tracking_row == 1) { ?>
													<?php if ($iteration == 1) { ?>
														<div>
															<b class="mdi mdi-checkbox-blank-circle text-danger"></b>
															<?php //echo str_replace("(...)", $val->keberadaan_surat, strtolower($tracking_basic[$val->id_agenda][$tracking_row]));?>
															<?php echo $tracking_format[$val->id_agenda][$tracking_row][$keberadaan_surat[$val->id_jabatan][$iteration]]; ?>
															<?php //echo '$val->id_agenda: "'.$val->id_agenda.'", $tracking_row: "'.$tracking_row.'", $val->keberadaan_surat: "'.$val->keberadaan_surat.'"<br>';?>
															<?php //$tracking_row[$val->tracking_order] ?>
															<?php //echo '<br>'; echo $tracking_format[$val->id_agenda][$tracking_row]; //print_r ($tracking_format); ?>
														</div>
													<?php } else { ?> 
														<div class="mdi mdi-ray-end-arrow mdi-rotate-90"></div>
														<div class="mdi mdi-check-circle">
															<?php echo $tracking_format[$val->id_agenda][$tracking_row][$val->keberadaan_surat]; ?>
															<?php //echo '$val->id_agenda: "'.$val->id_agenda.'", $tracking_row: "'.$tracking_row.'", $val->keberadaan_surat: "'.$val->keberadaan_surat.'"<br>'; echo $tracking_format[$val->id_agenda][$tracking_row]['kepalakantor']; ?>
															<?php //echo str_replace("(...)", $val->keberadaan_surat, strtolower($tracking_basic[$val->id_agenda][$tracking_row]));?>																
														</div>
													<?php }
													
													//echo $tracking_row[$val->id_agenda].'<br>';
													echo '$val->id_agenda: "'.$val->id_agenda.'", $iteration: "'.$iteration.'", $tracking_order: "'.$tracking_order[$val->id_agenda][$iteration].'"<br>';
													
													if ($tracking_order[$val->id_agenda][$iteration] == 4) {
														$tracking_order[$val->id_agenda][$iteration] = 2;
													} else {
														echo '$tracking_order[$val->id_agenda][$iteration] (before): '.$tracking_order[$val->id_agenda][$iteration].'<br>';	
														$tracking_order[$val->id_agenda][$iteration]--;
														echo '$tracking_order[$val->id_agenda][$iteration] (after): '.$tracking_order[$val->id_agenda][$iteration].'<br>';	
													}
													echo '$tracking_order[$val->id_agenda][$iteration] (aftermath): '.$tracking_order[$val->id_agenda][$iteration].'<br>';	
													$iteration++;
													//$tracking_row++;
													//print_r($tracking_format);
													print_r($tracking_order);
													//ekspektasi tracking format, misal dari trc_arsip
												} ?>
											</div>
											<div class="modal-footer">
												<button id="btnIsMicro" type="button" class="btn btn-square btn-danger" data-dismiss="modal" title="<?php echo 'Close';?>">Close&nbsp;&nbsp;<i class="fa fa-close"></i></button>
											</div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- /.modal -->
							</td>
							<td>
								<div align="center">
									<a onclick="return confirm('<?php echo $lang_are_you_sure;?>?');" href="<?php echo base_url($this->uri->rsegment(1).'/delete/'.$val->id_agenda);?>" title="<?php echo $lang_delete;?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
								</div>
							</td>
						<?php 
							}
							$no++;
							endforeach;
						?>	                	
                	</tbody>
				</table>
            </div>
            <!-- /.box-body -->
        </div>
          <!-- /.box -->
        <!-- /.col -->
    </div>
 </div>    