<?php
class Ap_surat_jenis_wajib_jawab extends MY_Model
{

    protected $_table_name = 'ap_surat_jenis_wajib_jawab';
    protected $_order_by = 'id_jenis_wajib_jawab';
    protected $_primary_key = 'id_jenis_wajib_jawab';
    protected $_primary_filter = 'intval';
   

    function __construct ()
    {
        parent::__construct();
    }

    public function get_new(){
        $variabel = new stdClass();
        $variabel->id_jenis_wajib_jawab='';
        $variabel->uraian_jenis_wajib_jawab='';

        return $variabel;
    }


}
