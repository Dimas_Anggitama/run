<?php
class Ap_surat_masuk2_m extends MY_Model {

    protected $_table_name = 'ap_surat_masuk2';
    protected $_order_by = 'id_agenda';
    protected $_primary_key = 'id_agenda';
    protected $_primary_filter = 'intval';

    function __construct() {
        parent::__construct();
    }

    public function get_new(){
        $variabel = new stdClass();
        $variabel->id_agenda='';
		$variabel->rules_basic_id='';			
        $variabel->id_jenis_surat='';	
        $variabel->catatan='';
		$variabel->keberadaan_surat='';
		$variabel->tracking='';
		$variabel->tanggal_disposisi='';
		$variabel->tanggal_dilihat='';
		$variabel->tanggal_dijawab='';
		$variabel->tanggal_arsip='';

        return $variabel;
    }

	public function get_all(){
		return $this->db->get('ap_surat_masuk2');
	}
	
	public function get_all_with($orderBy, $orderDirection, $table){
		$this->db->order_by($orderBy, $orderDirection);
		return $this->db->get($table);
	}
	
	public function get_jabatanKasi($id_jabatan){
		$this->db->where('id_jabatan', $id_jabatan);
		$this->db->select('uraian_jabatan'); 
		return $this->db->get('ap_jabatan_ref'); 
	}
	
	public function get_uraian($id, $uraian_kolom, $nama_tabel){
		$this->db->where($id);
		$this->db->select($uraian_kolom); 
		return $this->db->get($nama_tabel); 
	}
	
	public function get_surat($uraian_jabatan, $nama_tabel){
		$this->db->where('keberadaan_surat', $uraian_jabatan);
		return $this->db->get($nama_tabel); 
	}
	
	public function get_tracking_basic() {
		return $this->db->get('ap_surat_tracking'); 
	}
	
	//get tanggal arsip dan (tanggal jawab (disposisi))
	public function get_tanggal_disposisi($id_agenda, $nama_kolom, $nama_tabel) {
		// $this->db->where(array($nama_kolom.' IS NULL' => NULL));
		// return $this->db->get($nama_tabel);
		return $this->db->query("SELECT * FROM ".$nama_tabel." WHERE ".$nama_kolom." IS NULL AND id_agenda = ".$id_agenda);
    }
	
	public function get_tracking($tanggal, $uraian_tracking, $id_agenda) {
		return $this->db->query("SELECT CONCAT('(', ".$tanggal.", ') ".$uraian_tracking." ', keberadaan_surat, ' / `nama orang`') AS tracking_with_format FROM ap_surat_masuk2 where id_agenda = ".$id_agenda);

		//return $this->db->query("SELECT * FROM ".$nama_tabel." WHERE ".$nama_kolom." IS NULL AND id_agenda = ".$id_agenda);
    }
	
	public function post_tanggal_arsip_suratmasuk2($id_agenda, $tanggal_arsip){
		$this->db->where('id_agenda', $id_agenda);
		$this->db->update('ap_surat_masuk2', array('tanggal_arsip' => $tanggal_arsip));
	}
	
	public function post_uraian_suratmasuk2($id_agenda, $uraian){
		$this->db->where('id_agenda', $id_agenda);
		$this->db->update('ap_surat_masuk2', array('uraian_jenis_surat' => $uraian));
	}
	 
	public function post_suratmasuk2($data){
		$this->db->insert('ap_surat_masuk2', $data);
		//print_r($data);
		// $this->db->insert('quotes');
		// print_r($this->db->error());
		// exit;
	}
	
	public function update_suratmasuk2($data, $id_agenda){
		$this->db->where('id_agenda', $id_agenda);
		$this->db->update('ap_surat_masuk2', $data);
	}
	
	
}
