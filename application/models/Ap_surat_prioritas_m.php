<?php
class Ap_surat_prioritas_m extends MY_Model
{

    protected $_table_name = 'ap_surat_prioritas';
    protected $_order_by = 'id_prioritas';
    protected $_primary_key = 'id_prioritas';
    protected $_primary_filter = 'intval';
    protected $_timestamps = FALSE;
    public $rules = array(
        'nama_prioritas' => array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|required'
        )
    );

    function __construct ()
    {
        parent::__construct();
    }

    public function get_new(){
        $variabel = new stdClass();
        $variabel->id_prioritas='';
        $variabel->nama_prioritas='';

        return $variabel;
    }


}
