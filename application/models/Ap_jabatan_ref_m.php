<?php
class Ap_jabatan_ref_m extends MY_Model
{
    
    protected $_table_name = 'ap_jabatan_ref';
    protected $_order_by = 'id_jabatan';

    
    function __construct ()
    {
        parent::__construct();
    }
    
    public function get_count_that(){
        return $this->db->get('ap_jabatan_ref')->num_rows();
    }
}