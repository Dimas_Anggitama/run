<?php
class Ap_surat_jenis_arsip extends MY_Model
{

    protected $_table_name = 'ap_surat_jenis_arsip';
    protected $_order_by = 'id_jenis_arsip';
    protected $_primary_key = 'id_jenis_arsip';
    function __construct ()
    {
        parent::__construct();
    }

    public function get_new(){
        $variabel = new stdClass();
        $variabel->id_jenis_arsip='';
        $variabel->uraian_jenis_arsip='';

        return $variabel;
    }


}
