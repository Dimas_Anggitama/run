<?php
class Ap_surat_masuk_m extends MY_Model
{

    protected $_table_name = 'ap_surat_masuk';
    protected $_order_by = 'id_agenda';
    protected $_primary_key = 'id_agenda';
    protected $_primary_filter = 'intval';
    protected $_timestamps = FALSE;
    public $rules = array(
        'id_agenda' => array(
            'field' => 'id_agenda',
            'label' => 'Nomor',
            'rules' => 'trim|required'
        )
    );

    function __construct ()
    {
        parent::__construct();
    }

    public function get_new(){
        $variabel = new stdClass();
        $variabel->id_agenda='';
        $variabel->nomor_naskah='';
        $variabel->tgl_naskah='';
        $variabel->lampiran_naskah='';
        $variabel->id_sifat_kategori='';
        $variabel->id_sifat2='';
        $variabel->id_jenis_kategori='';
        $variabel->id_status='';
        $variabel->dari_naskah='';
        $variabel->perihal_naskah='';
        $variabel->file_surat='';
        $variabel->date_create_surat='';
        $variabel->nomor_agenda='';

        return $variabel;
    }
	
	public function deleteSuratMasuk($column, $id) {
		$this->db->where($column, $id);
		$this->db->delete($this->_table_name);
	}
	
	public function getSuratMasuk_frontDesk() {
		// select *
       // FROM ap_surat_masuk
       // ORDER BY LENGTH(id_agenda)
		//$this->db->order_by('title', 'DESC');
		
		// $this->db->like('id_agenda', 'frontdesk');
		$this->db->order_by('LENGTH(id_agenda)', 'DESC');
		return $this->db->get('ap_surat_masuk');
	}
	
	public function get_idAgenda($data){
		$this->db->select('id_agenda');		
		return $this->db->get_where('ap_surat_masuk', $data);
	}
    
	public function get_uraian($id, $nama_kolom, $nama_tabel){
		$this->db->select($nama_kolom);	
		$this->db->where($id);	
		return $this->db->get($nama_tabel);
	}

}
