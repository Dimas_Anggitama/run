<?php
class Ap_surat_masuk3_m extends MY_Model
{

    protected $_table_name = 'ap_surat_masuk2';
    protected $_order_by = 'id_distribusi';
    protected $_primary_key = 'id_distribusi';
    protected $_primary_filter = 'intval';

    function __construct ()
    {
        parent::__construct();
    }

    public function get_new(){
        $variabel = new stdClass();
        $variabel->id_distribusi='';
        $variabel->disposisi_kepada='';		
        $variabel->ket_disposisi='';
        $variabel->catatan='';

        return $variabel;
    }



}
