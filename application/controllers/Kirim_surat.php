<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kirim_surat extends Goodsyst_Controller {
	public function __construct() {
		parent::__construct();
		
		
		$this->load->model('ap_surat_sifat_m');
		$this->load->model('ap_surat_prioritas_m');
		$this->load->model('ap_surat_jenis_m');
		$this->load->model('ap_surat_status_m');
		$this->load->model('ap_surat_masuk_m');
		$this->load->model('ap_surat_masuk2_m');
	}
	
	public function disposisi($id) {	
		//$id merupakan ID dari surat masuk bagian frontdesk. 
		//Mengapa id-nya beda? 
		//	Karena data pada bagian frontdesk masih belum 'fix', yang berarti datanya masih bisa diperbaiki/dihapus kembali. 
		//	Sehingga tidak mengganggu/memperumit penomoran ID surat masuk yang telah 'fix' untuk diproses.
		
		//if contained "temp", then...
		$row_result = 0;
		
		foreach ($this->ap_surat_masuk2_m->get() as $row) {
			$id_agenda = $row->id_agenda;
			$row_result++;
		}
		
		if ($row_result == 0) {
			$id_agenda = date('Y').'0001';
		} else {
			//Mengambil id_agenda sebelumnya, memecah antara tahun dengan nomor ID, melakukan inkremen, dan menggabungkan kembali dengan tahun.
			$present_year = substr($id_agenda, 0, 4);	//ambil karakter 4 mulai dari karakter ke-0. misal: 20190001 menjadi 2019.
			$incremental_id = substr($id_agenda, 4);
			$incremental_id_array = str_split($incremental_id);	//split setiap karakter dalam satu string menjadi array
			$incremental_id_array_lastIndex = count($incremental_id_array) - 1;
			for ($i = 0; $i < count($incremental_id_array); $i++) {	//inkremen id.
				if ($incremental_id_array[$i] == 0) {
					continue;
				} else {
					if ($present_year == date('Y')) {	//jika tahun di database masih tahun ini
						$incremental_id_array[$incremental_id_array_lastIndex]++;
					} else {	//jika tahun di database sudah tahun lalu
						$present_year = date('Y');
						$incremental_id_array[$incremental_id_array_lastIndex] = 1;
					}
					break;
				}
			}
			$incremental_id = implode("", $incremental_id_array);
			
			$id_agenda = $present_year.$incremental_id;

			//1. find id
			//2. potong string
			//3. parsing ke int
			//4. inkremen
			//5. parsing ke string
			//6. tambahin ke id_agenda lagi
		}
		
		//pada tabel "ap_surat_masuk", melakukan update id_agenda:
		$this->ap_surat_masuk_m->updateData('id_agenda', $id, 'ap_surat_masuk', array('id_agenda' => $id_agenda)); //sebelum menjalankan ini, ubah sedikit kode di perekaman front desk, dimana hanya menampilkan dan menambah data menggunakan id yang memiliki kata kunci "frontdesk".
		
		//pada tabel "ap_surat_masuk2", melakukan post data:
		$data = array(
			'id_agenda' => $id_agenda,
			'id_tracking' => '0102',	// --> khusus baris ini, lihat panduannya di database dengan tabel "ap_surat_tracking".
			'tanggalDisposisi_kepalaKantor' => date('Y-m-d')
		);
		$this->ap_surat_masuk2_m->postData('ap_surat_masuk2', $data);
		
		//Pada akhirnya...
		$this->session->set_flashdata('message', 'Data berhasil didisposisikan ke Kepala Kantor');
		redirect($this->uri->rsegment(1) . '/index'); 
	}
	
    public function edit($id = NULL) {
		$jeneng=NULL;
        if (! empty($_FILES['file_surat']['name'])) {
            $jeneng = $this->nama_file($_FILES['file_surat']['name']);
        }
		
		$no_agenda = NULL;
		$row_result = 0;
		$tahun=intval(date('Y'));
		$this->data['contentToEdit'][0] = '';
		// $this->data['contentToEdit_namaStatus'][0] = '';
		//echo ($this->data['contentToEdit'] == );

		if ($this->db->query("SELECT COUNT(*) FROM tahun_surat")->row() != NULL) {
			$this->data['induk_tahun_surat'] = $this->db->query("SELECT * FROM ap_surat_masuk")->result();
			if ($this->data['induk_tahun_surat'] == NULL){
				$this->ap_surat_masuk_m->save('tahun_surat',$tahun);
				$no_agenda = 1;
			} else if ($this->data['induk_tahun_surat'] != NULL) {
				$temp = $this->db->query("SELECT COUNT(*) FROM ap_surat_masuk where tahun_surat=".$tahun."")->row();
				$no_agenda=$temp+1;
			}
		}
			
		$this->data['induk_sifat'] 		= $this->ap_surat_sifat_m->get();
		$this->data['induk_prioritas'] 	= $this->ap_surat_prioritas_m->get();
		$this->data['induk_jenis'] 		= $this->ap_surat_jenis_m->get();
		$this->data['induk_status'] 	= $this->ap_surat_status_m->get();
		
		//Apabila melakukan edit data surat masuk, menampilkan form input dengan data terletak di dalam form.
		if ($id != null) {
			
			//Data tambahan untuk di menu pilihan "Status", "Sifat", dan "Jenis". Dikarenakan "id_(...)" bernilai angka identitas menu, maka untuk ditampilkan di menu diperlukan nama/tulisan menunya agar dapat dipahami oleh pengguna.
			$this->data['contentToEdit'] = $this->ap_surat_masuk_m->get_uraian(array('id_agenda' => $id), '*', 'ap_surat_masuk')->result();
			foreach ($this->data['contentToEdit'] as $res) {
				//(1). data tambahan pertama: nama_status
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_status' => $res->id_status), 'nama_status', 'ap_surat_status')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaStatus'] = $val->nama_status;
				}
				
				//(2). data tambahan ke-2: nama_sifat_kategori
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_sifat_kategori' => $res->id_sifat_kategori), 'nama_sifat_kategori', 'ap_surat_sifat')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaSifatKategori'] = $val->nama_sifat_kategori;
				}
			
				//(3). data tambahan ke-3: nama_jenis_kategori
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_jenis_kategori' => $res->id_jenis_kategori), 'nama_jenis_kategori', 'ap_surat_jenis')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaJenisKategori'] = $val->nama_jenis_kategori;
				}
			
				//(4). data tambahan ke-4: nama_prioritas (prioritas naskah/surat)
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_prioritas' => $res->id_prioritas), 'nama_prioritas', 'ap_surat_prioritas')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaPrioritas'] = $val->nama_prioritas;
				}
			}
		}
		
		if ($this->input->post('surat') != NULL) {	//Jika melakukan submit pada form:
			// $no = $this->input->post('nomor_naskah');
			// echo '<script type="text/javascript">alert("'.$no.'")</script>';
			$data = array(
				'nomor_naskah' 		=> $this->input->post('nomor_naskah'),
                'tgl_naskah' 		=> $this->input->post('tanggal_naskah'),
                'lampiran_naskah' 	=> $this->input->post('lampiran_naskah'),
                'id_sifat_kategori'	=> $this->input->post('sifat_kategori'),
				'id_prioritas' 		=> $this->input->post('prioritas_kategori'),
                'id_jenis_kategori' => $this->input->post('jenis_kategori'),
                'id_status' 		=> $this->input->post('status'),
                'dari_naskah' 		=> $this->input->post('dari_naskah'),
                'file_surat' 		=> $jeneng,
                'perihal_naskah' 	=> $this->input->post('perihal_naskah'),
                'tgl_diterima' 		=> $this->input->post('tanggal_diterima'),
                'nomor_agenda' 		=> $no_agenda,
				'tahun_surat' 		=> $tahun
            );
            if (! empty($_FILES['file_surat']['name'])) {
                $this->upload_dokumen($jeneng, 'file_surat');
                if ($id) {
                    unlink('./uploads/' . $this->data['content']->file_surat);
                }
            }
            //$this->ap_surat_masuk_m->save($data);
			
			if ($data['tgl_naskah'] > $data['tgl_diterima']) {	//apabila form tanggal naskah dinas > tanggal diterima
				$this->data['message'] = 'Tanggal Naskah Dinas <b class="text-danger">tidak boleh melebihi</b> Tanggal Diterima!';
				
				//(1). get: nama_status
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_status' => $data['id_status']), 'nama_status', 'ap_surat_status')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaStatus'] = $val->nama_status;
				}
					
				//(2). get: nama_prioritas
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_prioritas' => $data['id_prioritas']), 'nama_prioritas', 'ap_surat_prioritas')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaPrioritas'] = $val->nama_prioritas;
				}
				
				//(3). get: nama_jenis_kategori
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_jenis_kategori' => $data['id_jenis_kategori']), 'nama_jenis_kategori', 'ap_surat_jenis')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaJenisKategori'] = $val->nama_jenis_kategori;
				}
				
				//(4). get: nama_sifat_kategori
				$result = $this->ap_surat_masuk_m->get_uraian(array('id_sifat_kategori' => $data['id_sifat_kategori']), 'nama_sifat_kategori', 'ap_surat_sifat')->result();
				foreach ($result as $val) {
					$this->data['contentToEdit_namaSifatKategori'] = $val->nama_sifat_kategori;
				}	
					
				$this->data['contentToEdit'][0] = (object) array(
					'nomor_naskah' 			=> $data['nomor_naskah'],
					'tgl_naskah' 			=> $data['tgl_naskah'], 
					'perihal_naskah' 		=> $data['perihal_naskah'],
					'dari_naskah' 			=> $data['dari_naskah'],
					'id_status'				=> $data['id_status'],
					'id_prioritas'			=> $data['id_prioritas'],
					'id_jenis_kategori' 	=> $data['id_jenis_kategori'],
					'id_sifat_kategori' 	=> $data['id_sifat_kategori'],
					'lampiran_naskah' 		=> $data['lampiran_naskah'],
					'file_surat' 			=> $data['file_surat'],
					'tgl_diterima' 			=> $data['tgl_diterima']
				);
			} else if ($id == null) {	//Apabila melakukan penambahan surat masuk (bukan melakukan edit data):
				$row_result = 0;
				foreach ($this->ap_surat_masuk_m->get(NULL, FALSE, 'LENGTH(id_agenda)') as $row) {
					$id_agenda = $row->id_agenda;
					$row_result++;
				}
				
				if ($row_result == 0) {
					$id_agenda = 'frontdesk-1';
				} else {				
					//mencari kandungan angka bilangan bulat (0-9). jika ketemu, maka temukan nilai posisi bilangan bulat tersebut dalam suatu string.
					for ($i = 0; $i <= 9; $i++) {	//perulangan for dimulai dari 0-9 karena angka bilangan bulat berupa 0-9.
						$temp_FOPEN[$i] = strpos($id_agenda, strval($i));	//FOPEN -> First Ocurrence Position Each Number
					}
					
					//mencari nilai yang paling rendah dari setiap nilai posisi bilangan bulat tersebut.
					//misal: id = "abcdefghi-501"
					//		 jika diurutkan dari 0, maka angka '5' berada di posisi/urutan ke-10.
					//		 sehingga urutan kemunculan angka berturut-turut berada di posisi/urutan ke-10, 11, dan 12.
					//		 karena ingin mengambil angka "501", maka perlu mendeteksi angka apa yang berada di posisi/urutan terdepan.
					//		 angka '5' merupakan angka yang berada di posisi/urutan terdepan (urutan ke-10).
					//		 sehingga dapat dilakukan pengambilan karakter dari suatu string dimulai dari urutan ke-10 sampai selesai.
					for ($i = 0; $i <= 9; $i++) {
						if ($temp_FOPEN[$i] == NULL) {
							continue;
						}	
						for ($j = 0; $j <= 9; $j++) {
							if ($temp_FOPEN[$j] == NULL) {
								continue;
							}
							if ($temp_FOPEN[$i] <= $temp_FOPEN[$j]) {
								$temp_LVP = $temp_FOPEN[$i];	//LVP -> Lowest Value Position
							}
							if ($FVP == NULL) {	//FVP -> Final Value Position
								$FVP = $temp_LVP;
							} else {
								if ($FVP > $temp_LVP) {
									$FVP = $temp_LVP;
								}
							}
						}
					}
					
					//mengambil ID angka menggunakan posisi/urutan yang telah didapat. kemudian menambahkan nilai '1' (inkremen ++ / +1) pada urutan ID angka tersebut.
					$incremental_id = substr($id_agenda, $FVP);
					$incremental_id++;
					
					//menambahkan string di depan ID yang telah ditambahkan agar ID kembali ke dalam format ID seperti seharusnya.
					$id_agenda = 'frontdesk-'.$incremental_id;
				}
				
				$data += array(
					'id_agenda' => $id_agenda
				);
				
				$this->session->set_flashdata('message', 'Data berhasil ditambahkan!');
				$this->ap_surat_masuk_m->postData('ap_surat_masuk', $data);
				redirect($this->uri->rsegment(1) . '/index');  
			} else {	//Apabila melakukan edit data surat masuk:
				$data += array(
					'id_agenda' => $id
				);
				
				$this->session->set_flashdata('message', 'Data berhasil diubah!');
				$this->ap_surat_masuk_m->updateData('id_agenda', $id, 'ap_surat_masuk', $data);
				redirect($this->uri->rsegment(1) . '/index');  
			}
		}
		
        $this->data['subview'] = $this->uri->rsegment(1) . '/edit';
        $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
        $this->load->view('_layout_main', $this->data);
	}
    
    public function index() {      
		$count = 1;
        //$this->db->order_by('id_agenda',"DESC");
        //$this->data['content'] = $this->ap_surat_masuk_m->get(); 
        $this->data['content'] = $this->ap_surat_masuk_m->get(NULL, FALSE, 'LENGTH(id_agenda)', NULL, 'similar', 'id_agenda', 'frontdesk'); 
		foreach ($this->data['content'] as $res) {	//ambil semua data
			//(1). data tambahan pertama: nama_sifat_kategori
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_sifat_kategori' => $res->id_sifat_kategori), 'nama_sifat_kategori', 'ap_surat_sifat')->result();
			foreach ($result as $val) {
				$this->data['nama_sifat_kategori'][$count] = $val->nama_sifat_kategori;
			}
			
			//(2). data tambahan ke-2: nama_prioritas
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_prioritas' => $res->id_prioritas), 'nama_prioritas', 'ap_surat_prioritas')->result();
			foreach ($result as $val) {
				$this->data['nama_prioritas'][$count] = $val->nama_prioritas;
			}
			
			//(3). data tambahan ke-3: nama_jenis_kategori
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_jenis_kategori' => $res->id_jenis_kategori), 'nama_jenis_kategori', 'ap_surat_jenis')->result();
			foreach ($result as $val) {
				$this->data['nama_jenis_kategori'][$count] = $val->nama_jenis_kategori;
			}
			
			//(4). data tambahan ke-4: nama_status
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_status' => $res->id_status), 'nama_status', 'ap_surat_status')->result();
			foreach ($result as $val) {
				$this->data['nama_status'][$count] = $val->nama_status;
			}
			$count++;
		}
			
        // $this->data['nama_sifat'] = $this->db->query("SELECT P.nama_sifat_kategori FROM ap_surat_sifat P JOIN ap_surat_masuk T ON P.id_sifat_kategori=T.id_sifat_kategori")->result();
        // $this->db->join('ap_surat_sifat', 'ap_surat_sifat.id_sifat_kategori=ap_surat_masuk.id_surat_kategori', 'right');
        // $this->data['induk_sifat'] = $this->ap_surat_sifat_m->get();
        // $this->db->select('*');
        // $this->db->from('ap_surat_sifat');
        // $this->db->join('ap_surat_masuk', 'ap_surat_masuk.id_sifat_kategori = ap_surat_sifat.id_sifat_kategori');
        // $this->db->where(array('ap_surat_masuk.id_sifat_kategori' == 'p_surat_sifat.id_sifat_kategori' ));

        $this->data['subview'] = $this->uri->rsegment(1) . '/index';
        $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
        $this->load->view('_layout_main', $this->data);
    }
	
	public function delete($id) {
        $this->ap_surat_masuk_m->deleteSuratMasuk('id_agenda', $id);
        redirect($this->uri->rsegment(1) . '/index/');
    }
}

// echo "<pre>";
	// print_r($data);
// echo "</pre>";

