<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kirim_surat2_arsip extends Goodsyst_Controller
{
 public function __construct()
  {
      parent::__construct();
	  $this->load->helper('form');
      $this->load->model('ap_surat_masuk_m');
      $this->load->model('ap_surat_masuk2_m');
	  $this->load->model('ap_jabatan_ref_m');
	  $this->load->model('ap_surat_jenis_arsip');
	  $this->load->model('ap_surat_jenis_wajib_jawab');
	  $this->load->model('ap_admin_m');
	  
  }

    public function edit ($id=NULL) {
			$data = array(
				'rules_basic_id' => '2',
				'catatan' => $this->input->post('catatan'),
				'tracking' => '2',
				'tanggal_dilihat' => NULL,
				'tanggal_dijawab' => NULL,
				'tanggal_disposisi' => NULL
			);
			$count=0;
			if($this->input->post('id_jenis_surat') > 10){
				$id = $this->input->post('id_jenis_surat') - 10;
				$where = array('id_jenis_arsip' => $id);
				$uraian = $this->ap_surat_masuk2_m->get_uraian($where,'uraian_jenis_arsip', 'ap_surat_jenis_arsip')->result();
				foreach($uraian as $res){
					$this->ap_surat_masuk2_m->post_uraian_suratmasuk2($this->input->post('hidden_idAgenda'), $res->uraian_jenis_arsip);
					$data['uraian_jenis_surat'] = $res->uraian_jenis_arsip;
				}
				
			}else{
				$data['tanggal_disposisi'] = $content->tgl_naskah==''?date('Y-m-d H:i:s'):date('Y-m-d H:i:s',strtotime($content->tgl_naskah));
				$id = $this->input->post('id_jenis_surat');
				$where = array('id_jenis_wajib_jawab' => $id);
				$uraian = $this->ap_surat_masuk2_m->get_uraian($where,'uraian_jenis_wajib_jawab', 'ap_surat_jenis_wajib_jawab')->result();
				foreach($uraian as $res){
					$this->ap_surat_masuk2_m->post_uraian_suratmasuk2($this->input->post('hidden_idAgenda'), $res->uraian_jenis_wajib_jawab);
					$data['uraian_jenis_surat'] = $res->uraian_jenis_wajib_jawab;
				}	
			}
			foreach ($this->input->post('id_jabatan') as $res) {
				if($this->input->post('id_jenis_surat') > 10){
					$data['tanggal_arsip'] = $content->tgl_naskah==''?date('Y-m-d H:i:s'):date('Y-m-d H:i:s',strtotime($content->tgl_naskah));
				}
				$jabatan_kasi = $this->ap_surat_masuk2_m->get_jabatanKasi($this->input->post('id_jabatan['.$count.']'))->result();
				foreach ($jabatan_kasi as $res){
					$data['keberadaan_surat'] = $res->uraian_jabatan;
				}
				if($count==0){
					$this->ap_surat_masuk2_m->update_suratmasuk2($data, $this->input->post('hidden_idAgenda'));
				}else{
					$data['id_agenda'] = $this->input->post('hidden_idAgenda');
					$this->ap_surat_masuk2_m->post_suratmasuk2($data);
				}
				$count++;
			} 
			redirect($this->uri->rsegment(1) . '/index');  
		 
		// $this->data['subview'] = $this->uri->rsegment(1) . '/index';
		// $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
		// $this->load->view('_layout_main', $this->data);
	}
    
    public function index() {      
		$count=1;
        $this->db->order_by('id_agenda',"DESC");
        $this->data['content'] = $this->ap_surat_masuk2_m->get();
		
		$result = $this->ap_surat_masuk2_m->get_all_with('tracking_order', 'DESC', 'ap_surat_tracking')->result();
		foreach ($result as $val) {
			$this->data['tracking_row'][$val->tracking_order] = $val->id_tracking;
			//echo '$val->tracking_order: "'.$val->tracking_order.'", $id_tracking: "'.$val->id_tracking.'"<br>';
		}
		print_r($this->data['tracking_row']); echo '<br>';
			
		$this->data['surat_masuk'] = $this->ap_surat_masuk_m->get();
		foreach ($this->data['surat_masuk'] as $res){
			
			$this->data['id_agenda'][$count] = $res->id_agenda;
			$this->data['nomor_naskah'][$count] = $res->nomor_naskah;
			$this->data['tgl_naskah'][$count] = $res->tgl_naskah;
			$this->data['lampiran_naskah'][$count] = $res->lampiran_naskah;
			//Sifat
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_sifat_kategori' => $res->id_sifat_kategori),'nama_sifat_kategori','ap_surat_sifat')->result();
			foreach($result as $val){
				$this->data['id_sifat_kategori'][$count] = $val->nama_sifat_kategori;
			}
			//Sifat kecepatan penyampaian
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_sifat2' => $res->id_sifat2),'nama_sifat2','ap_surat_sifat2')->result();
			foreach($result as $val){
				$this->data['id_sifat2'][$count] = $val->nama_sifat2;
			}
			//Jenis
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_jenis_kategori' => $res->id_jenis_kategori),'nama_jenis_kategori','ap_surat_jenis')->result();
			foreach($result as $val){
				$this->data['id_jenis_kategori'][$count] = $val->nama_jenis_kategori;
			}
			//Status
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_status' => $res->id_status),'nama_status','ap_surat_status')->result();
			foreach($result as $val){
				$this->data['id_status'][$count] = $val->nama_status;
			}
			$this->data['dari_naskah'][$count] = $res->dari_naskah;
			$this->data['perihal_naskah'][$count] = $res->perihal_naskah;
			$this->data['file_surat'][$count] = $res->file_surat;
			$this->data['date_create_surat'][$count] = $res->date_create_surat;
			$this->data['nomor_agenda'][$count] = $res->nomor_agenda;
			$this->data['tahun_surat'][$count] = $res->tahun_surat;
			
			//call id_tracking (keberadaan_surat) paling terkini dari DB surat_masuk2
			$result = $this->ap_surat_masuk2_m->get_uraian(array('id_agenda' => $res->id_agenda), 'tracking', 'ap_surat_masuk2')->result();
			foreach ($result as $val) {
				$id_tracking = $val->tracking;
			}
			//call uraian_tracking dari DB
			$result  = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
			foreach ($result as $val) {
				$this->data['uraian_tracking'][$count] = $val->status_tracking;	
			}
			//call warna status dari DB
			$result = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'warna', 'ap_surat_tracking')->result();
			foreach ($result as $val) {
				$this->data['warna'][$count] = $val->warna;
			}
			
			
			//$this->data['tracking_row'][$count][$id_tracking] = $tracking_order[$id_tracking];
			
			$tracking_row = 1;
			//OA + Kepala Kantor
			if ($id_tracking == 'trc_kk1') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
			} 
			
			//Arsip / Disposisi ke KASI
			else if ($id_tracking == 'trc_disposisi' | $id_tracking == 'trc_arsip') {
				$data['tracking_row'][$count] = 3;
				$keberadaan_surat_row = 1;	//untuk pembeda ketika id rows & tracking_row-nya masih sama pada row berikutnya.
				$tracking_roww = 3;
				$tracking_row_keberadaan_surat = 3;
				
				//(.1.1) Jika trc_disposisi
				if ($id_tracking == 'trc_disposisi') {
					$tracking_order[$count] = 4;
					//Call tracking with format: "(`datetime`) `uraian_tracking` `keberadaan_surat` / `nama orang`".
					$result = $this->ap_surat_masuk2_m->get_tracking('tanggal_disposisi', $this->data['uraian_tracking'][$count], $res->id_agenda)->result();
					foreach ($result as $val) {
						$this->data['tracking_format'][$count][$tracking_row] = $val->nama_kolom;
						//echo $this->data['tracking_format'][$count][$tracking_row];
						//$tracking_row++;
					}
				
				//(.1.2) Jika trc_arsip
				} else if ($id_tracking == 'trc_arsip') {
					$tracking_order[$count] = 3;
					//Call tracking with format: "(`datetime`) `uraian_tracking` `keberadaan_surat` / `nama orang`".
					$result = $this->ap_surat_masuk2_m->get_tracking('tanggal_arsip', $this->data['uraian_tracking'][$count], $res->id_agenda)->result();
					$result_row = 1;
					foreach ($result as $val) {
						$this->data['tracking_format'][$count][$tracking_row][$id_tracking][$keberadaan_surat[$result_row]] = $val->tracking_with_format;
						$result_row++;
						
						//ekspektasi tracking format
						//- - - - - - - -
						//id rows ($count)	$tracking_roww	$id_tracking	$keberadaan_surat_row	=	$keberadaan_surat	
						//1					3				trc_arsip		1							KASI PKC 1
						//1					3				trc_arsip		2							KASI PKC 2
						//1					3				trc_arsip		3							KASI PKC 3
						//1					2				trc_kk1			1							Kepala Kantor
						//1					1				trc_oa			1							Front Desk
						
						//ekspektasi keberadaan_surat
						//- - - - - - - -
						//id rows ($count)	$id_tracking	$keberadaan_surat_row	=	$keberadaan_surat	
						//1					trc_arsip		1							KASI PKC 1
						//1					trc_arsip		2							KASI PKC 2
						//1					trc_arsip		3							KASI PKC 3
						//1					trc_kk1			1							Kepala Kantor
						//1					trc_oa			1							Front Desk
					}
					
					//Keberadaan Surat
					// $result = $this->ap_surat_masuk2_m->get_uraian(array('id_agenda' => $res->id_agenda), 'keberadaan_surat', 'ap_surat_masuk2')->result();
					// foreach ($result as $val) {
						// $this->data['keberadaan_surat'][$count][$tracking_roww][$keberadaan_surat_row] = $val->keberadaan_surat;
						// echo 'tracking_roww: "'.$tracking_roww.'", keberadaan surat: "'.$this->data['keberadaan_surat'][$count][$tracking_roww][$keberadaan_surat_row].'", result_row: "'.$keberadaan_surat_row.', count: "'.$count.'"<br>';
						// $keberadaan_surat_row++;
						// //$tracking_row_count;
					// }
					
					$tracking_roww--;
				}
				
				// kenapa kalau ini dihapus malah ndak muncul? :
				// $status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				// foreach ($status_tracking as $status_trc) {
					// $this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					// $tracking_row++;
				// }
			//echo 'row = '.$tracking_row;
				/*
					$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
					foreach ($status_tracking as $status_trc) {
						$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					}
					$tracking_row++;
				*/
				//|
				//|
				//V
				// $result = $this->ap_surat_masuk2_m->get_tracking('[date_create_surat from surat masuk 1]', $this->data['uraian_tracking'][$count], $res->id_agenda)->result();

				//(.2) Tracking Row: 2, keberadaan surat: trc_kk1
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					//echo '$status_trc->status_tracking = "'.$status_trc->status_tracking.'", row = "'.$tracking_row.'"<br>';
					$result = $this->ap_surat_masuk2_m->get_tracking("'".$res->date_create_surat."'", $status_trc->status_tracking, $res->id_agenda)->result();
					// $result_row = 1;
					foreach ($result as $val) {
						$this->data['tracking_format'][$count][$tracking_row]['kepalakantor'] = $val->tracking_with_format;
	//echo '$val->tracking_with_format = "'.$val->tracking_with_format.'", row = "'.$tracking_row.'"<br>';
						// $result_row++;
					}
					//$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
				}
				
				//Keberadaan Surat
				// $result = $this->ap_surat_masuk2_m->get_uraian(array('id_agenda' => $res->id_agenda), 'keberadaan_surat', 'ap_surat_masuk2')->result();
				// foreach ($result as $val) {
					// $this->data['keberadaan_surat'][$count][$tracking_roww][$keberadaan_surat_row] = $val->keberadaan_surat;
					// echo 'tracking_roww: "'.$tracking_roww.'", keberadaan surat: "'.$this->data['keberadaan_surat'][$count][$tracking_roww][$keberadaan_surat_row].'", result_row: "'.$keberadaan_surat_row.', count: "'.$count.'"<br>';
					// $keberadaan_surat_row++;
				// }
				
				$tracking_roww--;
				
				//(.3) Tracking Row: 3, keberadaan surat: trc_oa
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
//echo '$status_trc->status_tracking = "'.$status_trc->status_tracking.'", row = "'.$tracking_row.'"<br>';
				}
				
				//Keberadaan Surat saat surat ini diterima
				$result = $this->ap_surat_masuk2_m->get_uraian(array('id_agenda' => $res->id_agenda), 'keberadaan_surat', 'ap_surat_masuk2')->result();
				foreach ($result as $val) {
					$this->data['keberadaan_surat'][$count][$keberadaan_surat_row] = $val->keberadaan_surat;
					echo 'keberadaan surat: "'.$this->data['keberadaan_surat'][$count][$keberadaan_surat_row].'", count: "'.$count.'", keberadaan_surat_row: "'.$keberadaan_surat_row.'"<br>';
					$this->data['tracking_order'][$count][$keberadaan_surat_row] = $tracking_order[$count];
					//echo '$count: "'.$count.'" $keberadaan_surat_row: "'.$keberadaan_surat_row.'", tracking order ($data): "'.$this->data['tracking_order'][$count][$keberadaan_surat_row].'"<br>';
					$keberadaan_surat_row++;
					//$tracking_row_keberadaan_surat--;
				}
			}
			
			//Dilihat KASI
			else if ($id_tracking == 'trc_lihat') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_disposisi'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
			} 
			
			//Dijawab KASI + kirim ke Kepala Kantor lagi
			else if ($id_tracking == 'trc_lihat') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_lihat'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_disposisi'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
			}
			
			//Dilihat Kepala Kantor setelah dijawab oleh KASI
			else if ($id_tracking == 'trc_kk2') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_jawab'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_lihat'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_disposisi'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$tracking_row] = $status_trc->status_tracking;
					$tracking_row++;
				}
			}
			
			$count++;
		}
		$this->data['induk_jabatan'] = $this->ap_jabatan_ref_m->get();
		$this->data['induk_jenis_arsip'] = $this->ap_surat_jenis_arsip->get();
		$this->data['induk_jenis_wajib'] = $this->ap_surat_jenis_wajib_jawab->get();
        $this->data['subview'] = $this->uri->rsegment(1) . '/index';
        $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
		$this->data['count_induk_jabatan'] = $this->ap_jabatan_ref_m->get_count_that();
        $this->load->view('_layout_main', $this->data);
    }
	
	 public function delete($id)
    {
        $this->ap_surat_masuk_m->delete($id);
        redirect($this->uri->rsegment(1) . '/index/');
    }
	
	public function getTracking_currentLocation() {
		
	}
	
	public function getTracking_tree() {
		
	}
}

