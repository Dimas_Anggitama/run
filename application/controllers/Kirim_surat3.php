<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kirim_surat3 extends Goodsyst_Controller
{
 public function __construct()
  {
      parent::__construct();
		
      $this->load->model('ap_surat_masuk_m');
      $this->load->model('ap_surat_masuk3_m');
	  $this->load->model('ap_jabatan_ref_m');
  }

    public function edit($id=NULL)
    {
		$no_agenda = NULL;
		$tahun=intval(date('Y'));

		if($this->db->query("SELECT COUNT(*) FROM tahun_surat")->row() != NULL){
			$this->data['induk_tahun_surat'] = $this->db->query("SELECT * FROM ap_surat_masuk")->result();
			if ($this->data['induk_tahun_surat'] == NULL){
				 $this->ap_surat_masuk_m->save('tahun_surat',$tahun);
				$no_agenda = 1;
			}elseif($this->data['induk_tahun_surat'] != NULL){
				//$tahun=intval(date('Y'));
				$temp= $this->db->query("SELECT COUNT(*) FROM ap_surat_masuk where tahun_surat=".$tahun."")->row();
				$temp=$temp-1;
				$query = $this->db->query("SELECT nomor_agenda FROM ap_surat_masuk LIMIT ".$temp.", 1")->row();
				$no_agenda=$query+1;
				//$this->data['induk_tahun_surat']->tahun_surat;
			}
		}
			
      $this->data['induk_sifat'] = $this->ap_surat_sifat_m->get();
      $this->data['induk_sifat2'] = $this->ap_surat_sifat2_m->get();
      $this->data['induk_jenis'] = $this->ap_surat_jenis_m->get();
      $this->data['induk_status'] = $this->ap_surat_status_m->get();
  if($this->input->post('surat') != NULL){
  $no=$this->input->post('nomor_naskah');
  echo '<script type="text/javascript">alert("'.$no.'")</script>';
     $data = array(
            'nomor_naskah' => $this->input->post('nomor_naskah'),
                       'tgl_naskah' => $this->input->post('date'),
                       'lampiran_naskah' => $this->input->post('lampiran_naskah'),
                       'id_sifat_kategori' => $this->input->post('sifat_kategori'),
             'id_sifat2' => $this->input->post('sifat2_kategori'),
                       'id_jenis_kategori' => $this->input->post('jenis_kategori'),
                       'id_status' => $this->input->post('status'),
                       'dari_naskah' => $this->input->post('dari_naskah'),
                       'file_surat' => $jeneng,
                       'perihal_naskah' => $this->input->post('perihal_naskah'),
                       'date_create_surat' => $this->input->post('tanggal'),
                       'nomor_agenda' => $no_agenda,
						'tahun_surat' => $tahun
            );
            if (! empty($_FILES['file_surat']['name'])) {
                $this->upload_dokumen($jeneng, 'file_surat');
                if ($id) {
                    unlink('./uploads/' . $this->data['content']->file_surat);
                }
            }
            $this->ap_surat_masuk_m->save($data);

            redirect($this->uri->rsegment(1) . '/index');  
  }

         $this->data['subview'] = $this->uri->rsegment(1) . '/edit';
        $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
        $this->load->view('_layout_main', $this->data);
}
    
     public function index()
    {      
        $this->db->order_by('id_agenda',"DESC");
        $this->data['content'] = $this->ap_surat_masuk_m->get(); 
        // $this->data['nama_sifat'] = $this->db->query("SELECT P.nama_sifat_kategori FROM ap_surat_sifat P JOIN ap_surat_masuk T ON P.id_sifat_kategori=T.id_sifat_kategori")->result();
        // $this->db->join('ap_surat_sifat', 'ap_surat_sifat.id_sifat_kategori=ap_surat_masuk.id_surat_kategori', 'right');
        // $this->data['induk_sifat'] = $this->ap_surat_sifat_m->get();
        // $this->db->select('*');
        // $this->db->from('ap_surat_sifat');
        // $this->db->join('ap_surat_masuk', 'ap_surat_masuk.id_sifat_kategori = ap_surat_sifat.id_sifat_kategori');
        // $this->db->where(array('ap_surat_masuk.id_sifat_kategori' == 'p_surat_sifat.id_sifat_kategori' ));

        $this->data['subview'] = $this->uri->rsegment(1) . '/index';
        $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
        $this->load->view('_layout_main', $this->data);
    }
	
	 public function delete($id)
    {
        $this->ap_surat_masuk_m->delete($id);
        redirect($this->uri->rsegment(1) . '/index/');
    }
	
	 public function tracking ($id)
    {
        $this->ap_surat_masuk_m->delete($id);
        redirect($this->uri->rsegment(1) . '/index/');
    }
	
	public function disposisi_ke_pelaksana($id_agenda){
		$data = array(
				'id_agenda' => $id_agenda,
                'rules_id' => '',
				'keberadaan_surat' => '',
				'tracking' => '1'
            );	
			$this->ap_surat_masuk2_m->save($data);
	}
}

