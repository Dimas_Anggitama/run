<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kirim_surat2_jawab extends Goodsyst_Controller
{
 public function __construct()
  {
      parent::__construct();
	  $this->load->helper('form');
      $this->load->model('ap_surat_masuk_m');
      $this->load->model('ap_surat_masuk2_m');
	  $this->load->model('ap_jabatan_ref_m');
	  $this->load->model('ap_surat_jenis_arsip');
	  $this->load->model('ap_surat_jenis_wajib_jawab');
	  $this->load->model('ap_admin_m');
	  
  }

    public function edit ($id=NULL) {
			$data = array(
				'rules_basic_id' => '2',
				'catatan' => $this->input->post('catatan'),
				'tanggal_dilihat' => NULL,
				'tanggal_dijawab' => NULL,
				'tanggal_disposisi' => NULL
			);
			$count=0;
			if($this->input->post('id_jenis_surat') > 10){
				$data['tracking'] = 'trc_arsip';
				$id = $this->input->post('id_jenis_surat') - 10;
				$where = array('id_jenis_arsip' => $id);
				$uraian = $this->ap_surat_masuk2_m->get_uraian($where,'uraian_jenis_arsip', 'ap_surat_jenis_arsip')->result();
				foreach($uraian as $res){
					$this->ap_surat_masuk2_m->post_uraian_suratmasuk2($this->input->post('hidden_idAgenda'), $res->uraian_jenis_arsip);
					$data['uraian_jenis_surat'] = $res->uraian_jenis_arsip;
				}
				
			}else{
				$data['tanggal_disposisi'] = $content->tgl_naskah==''?date('Y-m-d H:i:s'):date('Y-m-d H:i:s',strtotime($content->tgl_naskah));
				$data['tracking'] = 'trc_disposisi';
				$id = $this->input->post('id_jenis_surat');
				$where = array('id_jenis_wajib_jawab' => $id);
				$uraian = $this->ap_surat_masuk2_m->get_uraian($where,'uraian_jenis_wajib_jawab', 'ap_surat_jenis_wajib_jawab')->result();
				foreach($uraian as $res){
					$this->ap_surat_masuk2_m->post_uraian_suratmasuk2($this->input->post('hidden_idAgenda'), $res->uraian_jenis_wajib_jawab);
					$data['uraian_jenis_surat'] = $res->uraian_jenis_wajib_jawab;
				}
			}
			foreach ($this->input->post('id_jabatan') as $res) {
				if($this->input->post('id_jenis_surat') > 10){
					$data['tanggal_arsip'] = $content->tgl_naskah==''?date('Y-m-d H:i:s'):date('Y-m-d H:i:s',strtotime($content->tgl_naskah));
				}
				$jabatan_kasi = $this->ap_surat_masuk2_m->get_jabatanKasi($this->input->post('id_jabatan['.$count.']'))->result();
				foreach ($jabatan_kasi as $res){
					$data['keberadaan_surat'] = $res->uraian_jabatan;
				}
				if($count==0){
					$this->ap_surat_masuk2_m->update_suratmasuk2($data, $this->input->post('hidden_idAgenda'));
				}else{
					$data['id_agenda'] = $this->input->post('hidden_idAgenda');
					$this->ap_surat_masuk2_m->post_suratmasuk2($data);
				}
				$count++;
			} 
			redirect($this->uri->rsegment(1) . '/index');  
		 
		// $this->data['subview'] = $this->uri->rsegment(1) . '/index';
		// $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
		// $this->load->view('_layout_main', $this->data);
	}
    
    public function index()
    {      
		$count=1;
        $this->db->order_by('id_agenda',"DESC");
        $this->data['content'] = $this->ap_surat_masuk2_m->get();
		foreach($this->data['content'] as $res){
			$result = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $res->tracking), 'status_tracking', 'ap_surat_tracking')->result();
			foreach ($result as $val) {
				$this->data['uraian_tracking'][$count] = $val->status_tracking;
			}
			
			//call id_tracking dari DB
			$result = $this->ap_surat_masuk2_m->get_uraian(array('id_agenda' => $res->id_agenda), 'tracking', 'ap_surat_masuk2')->result();
			foreach ($result as $val) {
				$id_tracking = $val->tracking;
			}
			//call uraian_tracking dari DB
			$result  = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
			foreach ($result as $val) {
				$this->data['uraian_tracking'][$count] = $val->status_tracking;	
			}
			//call warna status dari DB
			$result = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'warna', 'ap_surat_tracking')->result();
			foreach ($result as $val) {
				$this->data['warna'][$count] = $val->warna;
			}
			
			$row = 1;
			//OA + Kepala Kantor
			if ($id_tracking == 'trc_kk1') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
			} 
			
			//Arsip / Disposisi ke KASI
			else if ($id_tracking == 'trc_disposisi' | $id_tracking == 'trc_arsip') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
			} 
			
			//Dilihat KASI
			else if ($id_tracking == 'trc_lihat') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_disposisi'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
			} 
			
			//Dijawab KASI + kirim ke Kepala Kantor lagi
			else if ($id_tracking == 'trc_lihat') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_lihat'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_disposisi'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
			}
			
			//Dilihat Kepala Kantor setelah dijawab oleh KASI
			else if ($id_tracking == 'trc_kk2') {
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => $id_tracking), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_jawab'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_lihat'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_disposisi'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_kk1'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
				$status_tracking = $this->ap_surat_masuk2_m->get_uraian(array('id_tracking' => 'trc_oa'), 'status_tracking', 'ap_surat_tracking')->result();
				foreach ($status_tracking as $status_trc) {
					$this->data['tracking_basic'][$count][$row] = $status_trc->status_tracking;
					$row++;
				}
			}
			
			$count++;
		}
		
		$count=1;
		$this->data['surat_masuk'] = $this->ap_surat_masuk_m->get();
		foreach ( $this->data['surat_masuk'] as $res){
			$this->data['id_agenda'][$count] = $res->id_agenda;
			$this->data['nomor_naskah'][$count] = $res->nomor_naskah;
			$this->data['tgl_naskah'][$count] = $res->tgl_naskah;
			$this->data['lampiran_naskah'][$count] = $res->lampiran_naskah;
			//Sifat
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_sifat_kategori' => $res->id_sifat_kategori),'nama_sifat_kategori','ap_surat_sifat')->result();
			foreach($result as $val){
				$this->data['id_sifat_kategori'][$count] = $val->nama_sifat_kategori;
			}
			//Sifat kecepatan penyampaian
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_sifat2' => $res->id_sifat2),'nama_sifat2','ap_surat_sifat2')->result();
			foreach($result as $val){
				$this->data['id_sifat2'][$count] = $val->nama_sifat2;
			}
			//Jenis
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_jenis_kategori' => $res->id_jenis_kategori),'nama_jenis_kategori','ap_surat_jenis')->result();
			foreach($result as $val){
				$this->data['id_jenis_kategori'][$count] = $val->nama_jenis_kategori;
			}
			//Status
			$result = $this->ap_surat_masuk_m->get_uraian(array('id_status' => $res->id_status),'nama_status','ap_surat_status')->result();
			foreach($result as $val){
				$this->data['id_status'][$count] = $val->nama_status;
			}
			$this->data['dari_naskah'][$count] = $res->dari_naskah;
			$this->data['perihal_naskah'][$count] = $res->perihal_naskah;
			$this->data['file_surat'][$count] = $res->file_surat;
			$this->data['date_create_surat'][$count] = $res->date_create_surat;
			$this->data['nomor_agenda'][$count] = $res->nomor_agenda;
			$this->data['tahun_surat'][$count] = $res->tahun_surat;
			
			$count++;
			
		}
		
		$this->data['induk_jabatan'] = $this->ap_jabatan_ref_m->get();
		$this->data['induk_jenis_arsip'] = $this->ap_surat_jenis_arsip->get();
		$this->data['induk_jenis_wajib'] = $this->ap_surat_jenis_wajib_jawab->get();
        $this->data['subview'] = $this->uri->rsegment(1) . '/index';
        $this->data['jscript'] = $this->uri->rsegment(1) . '/js';
		$this->data['count_induk_jabatan'] = $this->ap_jabatan_ref_m->get_count_that();
        $this->load->view('_layout_main', $this->data);
    }
	
	 public function delete($id)
    {
        $this->ap_surat_masuk_m->delete($id);
        redirect($this->uri->rsegment(1) . '/index/');
    }
	
	
}

